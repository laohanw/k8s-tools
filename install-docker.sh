#!/bin/bash

# 这个是用于每个node节点安装docker
# 安装必要的一些系统工具
yum install -y yum-utils device-mapper-persistent-data lvm2
# 添加软件源信息
sudo yum-config-manager --add-repo http://mirrors.aliyun.com/docker-ce/linux/centos/docker-ce.repo
# 更新并安装 Docker-CE
yum makecache fast
yum -y install docker-ce
# 开启Docker服务
service docker start
# 配置docker加速器
curl -sSL https://get.daocloud.io/daotools/set_mirror.sh | sh -s http://f1361db2.m.daocloud.io
# 重启docker
service docker restart

#离线安装docker-ce
#1、wget https://github.com/liumiaocn/easypack/blob/master/docker/install-docker.sh
#2、chmod +x install-docker.sh ||  ll install-docker.sh 
#3、curl -O https://download.docker.com/linux/static/stable/x86_64/docker-18.06.3-ce.tgz 
#4、ll install-docker.sh docker-18.06.3-ce.tgz 
#5、./install-docker.sh docker-18.06.3-ce.tgz 2>&1 |tee install.log