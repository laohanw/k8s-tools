#!/bin/bash
# 这个是用于每个node节点安装
if [ ! -d "/opt/kubernetes/bin" ]; then
  mkdir -p /opt/kubernetes/bin
fi
if [ ! -d "/opt/kubernetes/cfg" ]; then
  mkdir -p /opt/kubernetes/cfg
fi
if [ ! -d "/opt/kubernetes/ssl" ]; then
  mkdir -p /opt/kubernetes/ssl
fi
service flanneld stop
ETCD_ENDPOINTS="https://172.31.143.148:2379,https://172.31.143.146:2379,https://172.31.143.147:2379,https://172.31.143.149:2379"
# 写入分配到子网段到etcd，供flanneld使用
/opt/etcd/bin/etcdctl --ca-file=/opt/etcd/ssl/ca.pem \
--cert-file=/opt/etcd/ssl/server.pem \
--key-file=/opt/etcd/ssl/server-key.pem \
--endpoints=$ETCD_ENDPOINTS \
set /coreos.com/network/config '{"Network":"172.17.0.0/16","Backend":{"Type":"vxlan"}}'
# 下载flannel
if [ ! -f "/opt/kubernetes/bin/flanneld" ]; then
  wget https://github.com/coreos/flannel/releases/download/v0.11.0/flannel-v0.11.0-linux-amd64.tar.gz
  tar zxvf flannel-v0.11.0-linux-amd64.tar.gz
  mv flannel-v0.11.0-linux-amd64/{flanneld,mk-docker-opts.sh} /opt/kubernetes/bin/
  rm -rf flannel-v0.11.0-linux-amd64 flannel-v0.11.0-linux-amd64.tar.gz
fi

cat > /opt/kubernetes/cfg/flanneld <<EOF 
FLANNEL_OPTIONS="--etcd-endpoints=${ETCD_ENDPOINTS} \
-etcd-cafile=/opt/etcd/ssl/ca.pem \
-etcd-certfile=/opt/etcd/ssl/server.pem \
-etcd-keyfile=/opt/etcd/ssl/server-key.pem"
EOF

cat >/usr/lib/systemd/system/flanneld.service <<EOF
[Unit]
Description=Flanneld overlay address etcd agent
After=network-online.target network.target
Before=docker.service

[Service]
Type=notify
EnvironmentFile=/opt/kubernetes/cfg/flanneld
ExecStart=/opt/kubernetes/bin/flanneld --ip-masq \$FLANNEL_OPTIONS
ExecStartPost=/opt/kubernetes/bin/mk-docker-opts.sh -k DOCKER_NETWORK_OPTIONS -d /run/flannel/subnet.env
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

cat >/usr/lib/systemd/system/docker.service <<EOF
[Unit]
Description=Docker Application Container Engine
Documentation=https://docs.docker.com
After=network-onnline.target firewalld.service
Wants=network-online.target

[Service]
Type=notify
EnvironmentFile=/run/flannel/subnet.env
ExecStart=/usr/bin/dockerd \$DOCKER_NETWORK_OPTIONS
ExecReload=/bin/kill -s HUP \$MAINPID
LimitNOFILE=infinity
LimitNPROC=infinity
LimitCORE=infinity
TimeoutStartSec=0
Delegate=yes
KillMode=process
Restart=on-failure
StartLimitBurst=3
StartLimitInterval=60s

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable flanneld
systemctl start flanneld
sleep 2
systemctl status flanneld

echo "
#ps -ef |grep dockerd   查看docker是否挂接上flannel 其中--bip就是指定到flannel到ip
#如： root      3445     1  0 09:34 ?        00:00:00 /usr/bin/dockerd --bip=172.17.37.1/24 --ip-masq=false --mtu=1450
#route     查看路由是否添加来flannel
#ifconfig  查看是否多了一个flannel到虚拟网卡
#node之间互相ping下flannel虚拟网卡到ip，应该能ping通
#docker run -it busybox   进入container，使用#ip a 查看container内到ip，
#在docker 到container中互相ping内部到containerIP，应该能ping通
#注意：
#防火墙是否关闭
#flannel是否启动正常
"