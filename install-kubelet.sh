#!/bin/bash
# 用于在node上安装
NODE_ADDRESS=172.31.143.148
DNS_SERVER_IP=${2:-"10.0.0.2"}
WORK_DIR=/opt/kubernetes

service iptables stop 
service firewalld stop 
systemctl disable firewalld

if [ ! -d "$WORK_DIR/bin" ]; then
  mkdir -p $WORK_DIR/bin
fi
if [ ! -d "$WORK_DIR/cfg" ]; then
  mkdir -p $WORK_DIR/cfg
fi
if [ ! -d "$WORK_DIR/ssl" ]; then
  mkdir -p $WORK_DIR/ssl
fi
if [ ! -d "$WORK_DIR/logs" ]; then
  mkdir -p $WORK_DIR/logs
fi

systemctl stop kubelet

if [ `getenforce`  != 'Disabled' ]; then
  echo "you must close selinux ,/etc/selinux/config 修改selinux=disabled，重启电脑"
  exit 0
fi
echo "cat >$WORK_DIR/cfg/kubelet"
cat >$WORK_DIR/cfg/kubelet <<EOF
KUBELET_OPTS="--logtostderr=false \
--log-dir=$WORK_DIR/logs \
--v=4 \
--hostname-override=${NODE_ADDRESS} \
--kubeconfig=$WORK_DIR/cfg/kubelet.kubeconfig \
--bootstrap-kubeconfig=$WORK_DIR/cfg/bootstrap.kubeconfig \
--config=$WORK_DIR/cfg/kubelet.config \
--cert-dir=$WORK_DIR/ssl \
--pod-infra-container-image=registry.cn-hangzhou.aliyuncs.com/google-containers/pause-amd64:3.0"
EOF

echo "cat >$WORK_DIR/cfg/kubelet.config"
cat >$WORK_DIR/cfg/kubelet.config <<EOF
kind: KubeletConfiguration
apiVersion: kubelet.config.k8s.io/v1beta1
address: ${NODE_ADDRESS}
port: 10250
readOnlyPort: 10255
cgroupDriver: cgroupfs
clusterDNS:
- ${DNS_SERVER_IP}
clusterDomain: cluster.local.
failSwapOn: false
authentication:
  anonymous:
    enabled: true
EOF

echo "cat >/usr/lib/systemd/system/kubelet.service"
cat >/usr/lib/systemd/system/kubelet.service <<EOF
[Unit]
Description=Kubernetes Kubelet
After=docker.service
Requires=docker.service

[Service]
EnvironmentFile=$WORK_DIR/cfg/kubelet
ExecStart=$WORK_DIR/bin/kubelet \$KUBELET_OPTS
Restart=on-failure
KillMode=process

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable kubelet
systemctl start kubelet
sleep 2
systemctl status kubelet

