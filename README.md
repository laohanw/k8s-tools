# k8s-tools
### IP分配

节点		     公网IP				      私网IP          etcdName    type
master01		47.104.85.246		  172.31.143.146    etcd01
master02		47.105.215.128		172.31.143.147    etcd02
node01	    118.190.206.94		172.31.143.148    etcd03
node02      47.105.219.7      172.31.143.149    etcd04
nginx       47.105.215.231    172.31.143.150                master
nginx       47.105.218.224    172.31.143.151                backup

### k8s搭建的工具

1、在master上安装install-etcd.sh，拷贝配置到node上，运行

2、在每个node上安装install-docker.sh

3、在每个node上安装install-flannel.sh

4、在master上安装install-kubernetes.sh

5、在每个node上安装install-kubelet.sh

6、在每个node上安装install-kube-proxy.sh

### 测试

```
[]kubectl run nginx --image=nginx --replicas=1
[]kubectl get pods
  NAME                     READY   STATUS              RESTARTS   AGE
  nginx-7cdbd8cdc9-9d8mt   0/1     ContainerCreating   0          37s
[]kube expose deployment nginx --port=88 --target-port=80 --type=NodePort
[]kubectl get svc
  NAME         TYPE        CLUSTER-IP   EXTERNAL-IP   PORT(S)        AGE
  kubernetes   ClusterIP   10.0.0.1     <none>        443/TCP        4d5h
  nginx        NodePort    10.0.0.146   <none>        80:33554/TCP   7s
[]kubectl get pods -o wide
  NAME                     READY   STATUS    RESTARTS   AGE   IP            NODE            NOMINATED     NODE   READINESS GATES
  nginx-7cdbd8cdc9-9d8mt   1/1     Running   0          11m   172.17.81.2   192.168.1.111   <none>        <none>
```
node上可以内部ip测试访问nginx  curl 10.0.0.146:80
nginx分配到 192.168.1.111 node上
外部可以外部ip测试访问nginx   http://192.168.1.111:33554

###查看nginx日志
```
[]kubectl logs nginx-7cdbd8cdc9-9d8mt
  Error from server (Forbidden): Forbidden (user=system:anonymous, verb=get, resource=nodes, subresource=proxy) ( pods/log nginx-7cdbd8cdc9-9d8mt)
```
###需要加入权限,随意取个用户名字：cluster-system-anonymous
```
[]kubectl create clusterrolebinding cluster-system-anonymous --clusterrole=cluster-admin --user=system:anonymous
```
###再执行log日志查看
```
[]kubectl logs nginx-7cdbd8cdc9-9d8mt
10.0.0.146 - - [05/Apr/2019:09:21:10 +0000] "GET / HTTP/1.1" 200 612 "-" "curl/7.29.0" "-"
172.17.81.1 - - [05/Apr/2019:09:30:15 +0000] "GET / HTTP/1.1" 200 612 "-" "Mozilla/5.0 (Macintosh; Intel Mac OS X 10_13_6) AppleWebKit/605.1.15 (KHTML, like Gecko) Version/12.1 Safari/605.1.15" "-"
```

###配置UI,进入kubernetes下载的包里
```
cd kubernetes/
tar zxvf kubernetes-src.tar.gz
cd cluster/addons/dashboard/
kubectl create -f dashboard-configmap.yaml
kubectl create -f dashboard-rbac.yaml
kubectl create -f dashboard-secret.yaml
```

###修改dashboard-controller.yaml 的image镜像
```
image: registry.cn-hangzhou.aliyuncs.com/google_containers/kubernetes-dashboard-amd64:v1.10.0
```

###安装controller
```
kubectl create -f dashboard-controller.yaml
```

···
[ ]kubectl get pods -n kube-system
NAME                                    READY   STATUS    RESTARTS   AGE
kubernetes-dashboard-6bff7dc67d-ggt65   1/1     Running   0          2m28s
···

···
[ ]kubectl logs kubernetes-dashboard-6bff7dc67d-ggt65  -n kube-system
···

###修改为NodePort，暴露出去
```
vi dashboard-service.yaml
spec:
  type: NodePort
```

```
[]kubectl create -f dashboard-service.yaml
[]kubectl create -f /home/root/tools/k8s-admin.yaml
[]kubectl get secret -n kube-system
NAME                               TYPE                                  DATA   AGE
dashboard-admin-token-pv9gl        kubernetes.io/service-account-token   3      61s
default-token-57z2r                kubernetes.io/service-account-token   3      108m
kubernetes-dashboard-certs         Opaque                                0      16m
kubernetes-dashboard-key-holder    Opaque                                2      16m
kubernetes-dashboard-token-pff8j   kubernetes.io/service-account-token   3      16m
[] kubectl describe secret dashboard-admin-token-pv9gl -n kube-system
Name:         dashboard-admin-token-pv9gl
Namespace:    kube-system
Labels:       <none>
Annotations:  kubernetes.io/service-account.name: dashboard-admin
              kubernetes.io/service-account.uid: 178a2796-5f8d-11e9-be3a-00163e058a7b

Type:  kubernetes.io/service-account-token

Data
====
ca.crt:     1359 bytes
namespace:  11 bytes
token:      eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJkYXNoYm9hcmQtYWRtaW4tdG9rZW4tcHY5Z2wiLCJrdWJlcm5ldGVzLmlvL3NlcnZpY2VhY2NvdW50L3NlcnZpY2UtYWNjb3VudC5uYW1lIjoiZGFzaGJvYXJkLWFkbWluIiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9zZXJ2aWNlLWFjY291bnQudWlkIjoiMTc4YTI3OTYtNWY4ZC0xMWU5LWJlM2EtMDAxNjNlMDU4YTdiIiwic3ViIjoic3lzdGVtOnNlcnZpY2VhY2NvdW50Omt1YmUtc3lzdGVtOmRhc2hib2FyZC1hZG1pbiJ9.F3d-u2Is7dV1sgJMEQUI9StljG9b51M2GlgtWa7aUfwNBRuWNKeHtUotN4bBRZ9BanAZrzPAXeIyIRpGoMHzrB8wdo3qbAs1ZxT75j6z45rB8j3UmjbiFj6Czm5sJ8YPCb9b8Kp6HXaUh-9XtjOI3xNUf8ylWt679XUgDywsBotJwD_GuRNnd85gR6yzOHzgVJLAffsHkML-9A9k4dvact-GG-PFVjsV9aEiu5AUKKmazZTQdvbh8Sxz4ePSgBH-NsKu_EwrEKHwKlJ9IQ-7zxj-7H0MyuZDH0sKnY-IrcWltl5ZmU6UnR9oQ4o_tOVwZ3TTzVK_4utXH3uprfNNsA
```

复制token值登陆ui，至于复制是是否有空格或者回车

###搭建master02
在master01上操作
```
scp /opt/kubernetes/ root@172.31.143.147:/opt/
scp -r /opt/kubernetes/ root@172.31.143.147:/opt/
scp /usr/lib/systemd/system/{kube-apiserver,kube-scheduler,kube-controller-manager}.service root@172.31.143.147:/usr/lib/systemd/system/
```

在master02上操作
修改kube-apiserver 的IP地址为master02的ip


###搭建nginx作为负载均衡
访问www.nginx.org
documentation
install on linux
查找centos 复制
```
[nginx-stable]
name=nginx stable repo
baseurl=http://nginx.org/packages/centos/$releasever/$basearch/
gpgcheck=1
enabled=1
gpgkey=https://nginx.org/keys/nginx_signing.key
```

```
[]vi /etc/yum.repos.d/nginx.repo

[nginx-stable]
name=nginx stable repo
baseurl=http://nginx.org/packages/centos/7/$basearch/
gpgcheck=1
enabled=1
gpgkey=https://nginx.org/keys/nginx_signing.key

[] yum install -y nginx

[]nginx -V
[]vi /etc/nginx/nginx.conf
stream {
  log_format main '$remote_addr $upstream_addr - [$time_local] $status $upstream_bytes_sent';
  access_log /var/log/nginx/k8s-access.log main;
  upstream k8s-apiserver {
    server 172.31.143.146:6443;
    server 172.31.143.147:6443;
  }
  server {
    listen 172.31.143.150:6443;
    proxy_pass k8s-apiserver;
  }
}
```

修改node节点的kubeconfig的ip，kubelet.kubeconfig,bootstrap.kubeconfig,kube-proxy.kubeconfig的serverip为nginx的服务器的ip
```
vi kubelet.kubeconfig
vi kube-proxy.kubeconfig
vi bootstrap.kubeconfig

[]systemctl restart kubelet
[]systemctl restart kube-proxy
```

在nginx服务器上实时查看日志
```
[]tail -f /var/log/nginx/k8s-access.log
```
重启几个node节点的kubelet试下，nginx会收到kubelet重启的请求，并分配到指定的masterip上

##下面的主从nginx使用keepalived实现，需要使用虚拟IP，但云服务器不支持自建虚拟ip，固下面 的方法无法适用在阿里云等云服务器上
### 配置主从的nginx
操作和主nginx一样下载nginx，设置nginx.conf ，但listen地址使用从的IP地址 172.31.143.151

在主和从nginx上分别安装keepalived
```
[]yum install -y keepalived
```

传送keepalived.conf配置文件到nginx主服务器上
修改virtual_ipaddress 172.31.143.150/24 为自定义的虚拟ip地址
查看网卡信息
```
[]ip a
1: lo: <LOOPBACK,UP,LOWER_UP> mtu 65536 qdisc noqueue state UNKNOWN group default qlen 1000
    link/loopback 00:00:00:00:00:00 brd 00:00:00:00:00:00
    inet 127.0.0.1/8 scope host lo
       valid_lft forever preferred_lft forever
2: eth0: <BROADCAST,MULTICAST,UP,LOWER_UP> mtu 1500 qdisc pfifo_fast state UP group default qlen 1000
    link/ether 00:16:3e:04:b8:28 brd ff:ff:ff:ff:ff:ff
    inet 172.31.143.150/20 brd 172.31.143.255 scope global dynamic eth0
       valid_lft 315338421sec preferred_lft 315338421sec
```
修改interface 为eth0
传送check_nginx.sh文件到主nginx服务器的/usr/local/nginx/sbin/check_nginx.sh
添加执行权限
```
chmod +x /usr/local/nginx/sbin/check_nginx.sh
```

发送keepalive.conf和check_nginx.sh文件到从nginx服务器上
```
scp /etc/keepalived/keepalived.conf root@172.31.143.151:/etc/keepalived/keepalived.conf
scp /usr/local/nginx/sbin/check_nginx.sh root@172.31.143.151:/usr/local/nginx/sbin
```
修改从nginx服务器的keepalived.conf
保持virtual_ipaddress 172.31.143.150/24不变
修改state BACKUP
修改priority 90


### 添加ingress-nginx
查看github，kubernetes/ingress-nginx   查找deploy文件夹，找到文档说明

下载ingress-nginx配置文件
```
wget https://raw.githubusercontent.com/kubernetes/ingress-nginx/master/deploy/mandatory.yaml
```

修改文件中image为：lizhenliang/nginx-ingress-controller:0.20.1
spec下添加使用宿主机网络

```
hostNetwork: true
```

```
[]kubectl apply -f mandatory.yaml
```
