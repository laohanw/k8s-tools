/opt/etcd/bin/etcd \
--name="etcd01" \
--data-dir="/var/lib/etcd/default.etcd" \
--listen-peer-urls="https://172.31.143.148:2380" \
--listen-client-urls="https://172.31.143.148:2379,http://127.0.0.1:2379" \
--advertise-client-urls="https://172.31.143.148:2379" \
--initial-advertise-peer-urls="https://172.31.143.148:2380" \
--initial-cluster="etcd01=https://172.31.143.148:2380,etcd02=https://172.31.143.146:2380,etcd03=https://172.31.143.147:2380" \
--initial-cluster-token="etcd-cluster" \
--initial-cluster-state="new" \
--cert-file=/opt/etcd/ssl/server.pem \
--key-file=/opt/etcd/ssl/server-key.pem \
--peer-cert-file=/opt/etcd/ssl/server.pem \
--peer-key-file=/opt/etcd/ssl/server-key.pem \
--trusted-ca-file=/opt/etcd/ssl/ca.pem \
--peer-trusted-ca-file=/opt/etcd/ssl/ca.pem

/opt/etcd/bin/etcd \
--name="etcd02" \
--data-dir="/var/lib/etcd/default.etcd" \
--listen-peer-urls="https://172.31.143.146:2380" \
--listen-client-urls="https://172.31.143.146:2379,http://127.0.0.1:2379" \
--advertise-client-urls="https://172.31.143.146:2379" \
--initial-advertise-peer-urls="https://172.31.143.146:2380" \
--initial-cluster="etcd01=https://172.31.143.148:2380,etcd02=https://172.31.143.146:2380,etcd03=https://172.31.143.147:2380" \
--initial-cluster-token="etcd-cluster" \
--initial-cluster-state="new" \
--cert-file=/opt/etcd/ssl/server.pem \
--key-file=/opt/etcd/ssl/server-key.pem \
--peer-cert-file=/opt/etcd/ssl/server.pem \
--peer-key-file=/opt/etcd/ssl/server-key.pem \
--trusted-ca-file=/opt/etcd/ssl/ca.pem \
--peer-trusted-ca-file=/opt/etcd/ssl/ca.pem

/opt/etcd/bin/etcd \
--name="etcd03" \
--data-dir="/var/lib/etcd/default.etcd" \
--listen-peer-urls="https://172.31.143.147:2380" \
--listen-client-urls="https://172.31.143.147:2379,http://127.0.0.1:2379" \
--advertise-client-urls="https://172.31.143.147:2379" \
--initial-advertise-peer-urls="https://172.31.143.147:2380" \
--initial-cluster="etcd01=https://172.31.143.148:2380,etcd02=https://172.31.143.146:2380,etcd03=https://172.31.143.147:2380" \
--initial-cluster-token="etcd-cluster" \
--initial-cluster-state="new" \
--cert-file=/opt/etcd/ssl/server.pem \
--key-file=/opt/etcd/ssl/server-key.pem \
--peer-cert-file=/opt/etcd/ssl/server.pem \
--peer-key-file=/opt/etcd/ssl/server-key.pem \
--trusted-ca-file=/opt/etcd/ssl/ca.pem \
--peer-trusted-ca-file=/opt/etcd/ssl/ca.pem
eyJhbGciOiJSUzI1NiIsImtpZCI6IiJ9.eyJpc3MiOiJrdWJlcm5ldGVzL3NlcnZpY2VhY2NvdW50Iiwia3ViZXJuZXRlcy5pby9zZXJ2aWNlYWNjb3VudC9uYW1lc3BhY2UiOiJrdWJlLXN5c3RlbSIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VjcmV0Lm5hbWUiOiJrdWJlcm5ldGVzLWRhc2hib2FyZC10b2tlbi14bTg0cCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50Lm5hbWUiOiJrdWJlcm5ldGVzLWRhc2hib2FyZCIsImt1YmVybmV0ZXMuaW8vc2VydmljZWFjY291bnQvc2VydmljZS1hY2NvdW50LnVpZCI6IjA4MzA1ZjM0LTVlNjQtMTFlOS04NTQ3LTAwMTYzZTA2MGRmZSIsInN1YiI6InN5c3RlbTpzZXJ2aWNlYWNjb3VudDprdWJlLXN5c3RlbTprdWJlcm5ldGVzLWRhc2hib2FyZCJ9.R3tCI9YVT_u704tkZzzvpYT62KD5QT94yymZYKS8d1a6laEPhV2kw5pvo6TtRTHh2n4eE-etWz_iSFehqRQhCInOmXSfnge-qeuZDe7PAcgmwnBraMFvxrswtDlA-pnwesxEYeXaEJDbIprT7bWqqBAcOUVenUMqziGZqCYg9LsptSZg9LPlKtuiqh75IS84loolg1VkQYZJoNnkpsIA9WEEKkl97mxnOU-KNMpENg8i9-G_eiC3G8ANcHlouEXySpGBe9CkhwRzzUKllqthtq3bJPcLtyv3r2QHtJfc5i-3uT5eTPXYaUSx8ETGSEvz4AcvY96VQralsgNbMIhoiw