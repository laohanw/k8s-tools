#!/bin/bash
if [ `yum list installed | grep expect | wc -l` -ne 0 ]; then
  echo "expect is installed"
else
  yum -y install expect
fi
echo "install cfssl"
if [ ! -f "/usr/local/bin/cfssl" ]; then
curl -L https://pkg.cfssl.org/R1.2/cfssl_linux-amd64 -o /usr/local/bin/cfssl
fi
if [ ! -f "/usr/local/bin/cfssljson" ]; then
curl -L https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64 -o /usr/local/bin/cfssljson
fi
if [ ! -f "/usr/local/bin/cfssl-certinfo" ]; then
curl -L https://pkg.cfssl.org/R1.2/cfssl-certinfo_linux-amd64 -o /usr/local/bin/cfssl-certinfo
fi
chmod +x /usr/local/bin/cfssl /usr/local/bin/cfssljson /usr/local/bin/cfssl-certinfo


chmod +x ./install-etcd.sh
./install-etcd.sh etcd01 192.168.1.101 ["192.168.1.101","192.168.1.111","192.168.1.112"] https://192.168.1.101:2380,etcd02=https://192.168.1.111:2380,etcd03=https://192.168.1.112:2380