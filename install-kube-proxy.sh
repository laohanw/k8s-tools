
WORK_DIR=/opt/kubernetes
NODE_ADDRESS=172.31.143.148

systemctl stop kube-proxy

echo "cat > $WORK_DIR/cfg/kube-proxy"
cat > $WORK_DIR/cfg/kube-proxy <<EOF
KUBE_PROXY_OPTS="--logtostderr=false \
--log-dir=$WORK_DIR/logs \
--v=4 \
--hostname-override=$NODE_ADDRESS \
--cluster-cidr=10.0.0.0/24 \
--proxy-mode=ipvs \
--kubeconfig=$WORK_DIR/cfg/kube-proxy.kubeconfig"
EOF

cat >/usr/lib/systemd/system/kube-proxy.service <<EOF
[Unit]
Description=Kubernetes Proxy
After=network.target
[Service]
EnvironmentFile=-$WORK_DIR/cfg/kube-proxy
ExecStart=$WORK_DIR/bin/kube-proxy \$KUBE_PROXY_OPTS
Restart=on-failure
[Install]
WantedBy=multi-user.target
EOF
systemctl daemon-reload
systemctl enable kube-proxy
sleep 2
systemctl start kube-proxy