#!/bin/bash
#仅使用在master节点上，其它master需要在单个master上复制配置，不能直接通过这个代码安装
ETCD_NAME="etcd01"
ETCD_IP=172.31.143.146
ETCD_HOSTS=[\"172.31.143.148\",\"172.31.143.146\",\"172.31.143.147\"]
ETCD_CLUSTER=\"etcd01=https://172.31.143.146:2380,etcd02=https://172.31.143.147:2380,etcd03=https://172.31.143.148:2380,etcd04=https://172.31.143.149:2380\"
WORK_DIR=/opt/etcd

if [ ! -d "/opt/etcd/bin" ]; then
  mkdir -p /opt/etcd/bin
fi
if [ ! -d "/opt/etcd/cfg" ]; then
  mkdir -p /opt/etcd/cfg
fi
if [ ! -d "/opt/etcd/ssl" ]; then
  mkdir -p /opt/etcd/ssl
fi
if [ ! -d "/opt/etcd/logs" ]; then
  mkdir -p /opt/etcd/logs
fi

if [ `getenforce`  != 'Disabled' ]; then
  echo "you must close selinux ,/etc/selinux/config 修改selinux=disabled，重启电脑"
  exit 0
fi
echo "Debug mode to stop firewalld you must usr firewalld in production"
service iptables stop 
service firewalld stop 
systemctl disable firewalld

systemctl stop etcd

echo "ntpdate time.windows.com"
if [ `yum list installed | grep ntpdate | wc -l` -ne 0 ]; then
  echo "ntpdate is installed"
else
  yum -y install ntpdate
fi
ntpdate time.windows.com

echo "install cfssl"
if [ ! -f "/usr/local/bin/cfssl" ]; then
curl -L https://pkg.cfssl.org/R1.2/cfssl_linux-amd64 -o /usr/local/bin/cfssl
fi
if [ ! -f "/usr/local/bin/cfssljson" ]; then
curl -L https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64 -o /usr/local/bin/cfssljson
fi
if [ ! -f "/usr/local/bin/cfssl-certinfo" ]; then
curl -L https://pkg.cfssl.org/R1.2/cfssl-certinfo_linux-amd64 -o /usr/local/bin/cfssl-certinfo
fi
chmod +x /usr/local/bin/cfssl /usr/local/bin/cfssljson /usr/local/bin/cfssl-certinfo


if [ ! -f "/opt/etcd/bin/etcd" ]; then
  wget https://github.com/etcd-io/etcd/releases/download/v3.3.12/etcd-v3.3.12-linux-amd64.tar.gz
  tar zxvf etcd-v3.3.12-linux-amd64.tar.gz
  if [ ! -d "/opt/etcd/bin" ]; then
    mkdir -p /opt/etcd/bin
  fi
  mv etcd-v3.3.12-linux-amd64/{etcd,etcdctl} /opt/etcd/bin/
  rm -rf etcd-v3.3.12-linux-amd64.tar.gz
fi

echo "cat > ca-config.json"
cat > ca-config.json <<EOF
{
  "signing": {
    "default": {
      "expiry": "87600h"
    },
    "profiles": {
      "www": {
         "expiry": "87600h",
         "usages": [
            "signing",
            "key encipherment",
            "server auth",
            "client auth"
        ]
      }
    }
  }
}
EOF
echo "cat > ca-csr.json"
cat > ca-csr.json <<EOF
{
    "CN": "etcd CA",
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "L": "Beijing",
            "ST": "Beijing"
        }
    ]
}
EOF

echo "cfssl gencert -initca ca-csr.json | cfssljson -bare ca"
cfssl gencert -initca ca-csr.json | cfssljson -bare ca -

#--------------------------
echo "cat > server-csr.json"
cat > server-csr.json <<EOF
{
    "CN": "etcd",
    "hosts": $ETCD_HOSTS,
    "key": {
        "algo": "rsa",
        "size": 2048
    },
    "names": [
        {
            "C": "CN",
            "L": "BeiJing",
            "ST": "BeiJing"
        }
    ]
}
EOF

echo "cfssl gencert  -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=www server-csr.json | cfssljson -bare server"
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=www server-csr.json | cfssljson -bare server

echo "mv ca*pem server*pem /opt/etcd/ssl/"
mv ca*pem server*pem ca-config.json ca.csr ca-csr.json server.csr server-csr.json /opt/etcd/ssl/

echo "cat >${WORK_DIR}/cfg/etcd"
cat <<EOF >${WORK_DIR}/cfg/etcd
#[Member]
ETCD_NAME="${ETCD_NAME}"
ETCD_DATA_DIR="/var/lib/etcd/default.etcd"
ETCD_LISTEN_PEER_URLS="https://${ETCD_IP}:2380"
ETCD_LISTEN_CLIENT_URLS="https://${ETCD_IP}:2379"

#[Clustering]
ETCD_INITIAL_ADVERTISE_PEER_URLS="https://${ETCD_IP}:2380"
ETCD_ADVERTISE_CLIENT_URLS="https://${ETCD_IP}:2379"
ETCD_INITIAL_CLUSTER="${ETCD_CLUSTER}"
ETCD_INITIAL_CLUSTER_TOKEN="etcd-cluster"
ETCD_INITIAL_CLUSTER_STATE="new"
EOF

echo "cat >/usr/lib/systemd/system/etcd.service"
cat <<EOF >/usr/lib/systemd/system/etcd.service
[Unit]
Description=Etcd Server
After=network.target
After=network-online.target
Wants=network-online.target

[Service]
Type=notify
EnvironmentFile=${WORK_DIR}/cfg/etcd
ExecStart=${WORK_DIR}/bin/etcd \
--name=\${ETCD_NAME} \
--data-dir=\${ETCD_DATA_DIR} \
--listen-peer-urls=\${ETCD_LISTEN_PEER_URLS} \
--listen-client-urls=\${ETCD_LISTEN_CLIENT_URLS},http://127.0.0.1:2379 \
--advertise-client-urls=\${ETCD_ADVERTISE_CLIENT_URLS} \
--initial-advertise-peer-urls=\${ETCD_INITIAL_ADVERTISE_PEER_URLS} \
--initial-cluster=\${ETCD_INITIAL_CLUSTER} \
--initial-cluster-token=\${ETCD_INITIAL_CLUSTER_TOKEN} \
--initial-cluster-state=\${ETCD_INITIAL_CLUSTER_STATE} \
--cert-file=${WORK_DIR}/ssl/server.pem \
--key-file=${WORK_DIR}/ssl/server-key.pem \
--peer-cert-file=${WORK_DIR}/ssl/server.pem \
--peer-key-file=${WORK_DIR}/ssl/server-key.pem \
--trusted-ca-file=${WORK_DIR}/ssl/ca.pem \
--peer-trusted-ca-file=${WORK_DIR}/ssl/ca.pem
Restart=on-failure
LimitNOFILE=65536

[Install]
WantedBy=multi-user.target
EOF

echo "systemctl daemon-reload"
systemctl daemon-reload
echo "systemctl enable etcd"
systemctl enable etcd
echo "systemctl start etcd"
systemctl start etcd
sleep 2
systemctl status etcd

# -----------------------------------------------------
echo "
#拷贝 配置文件到node节点，/opt/etcd/*,/usr/lib/systemd/system/etcd ,
#修改/opt/etcd/cfg/etcd 配置 
#ETCD_NAME修改维当前node对应ETCD_INITIAL_CLUSTER中到名字如etcd02
#ETCD_LISTEN_PEER_URLS  修改为ETCD_INITIAL_CLUSTER中到对应到node到https到ip
#ETCD_LISTEN_CLIENT_URLS 修改为ETCD_INITIAL_CLUSTER中到对应到node到https到ip
#ETCD_INITIAL_ADVERTISE_PEER_URLS 修改为ETCD_INITIAL_CLUSTER中到对应到node到https到ip
#ETCD_INITIAL_CLUSTER不要修改
#systemctl daemon-reload | systemctl enable etcd | systemctl start etcd

#查看部署etcd是否成功 /opt/etcd/bin/etcdctl --ca-file=/opt/etcd/ssl/ca.pem --cert-file=/opt/etcd/ssl/server.pem --key-file=/opt/etcd/ssl/server-key.pem --endpoints="https://172.31.143.148:2379,https://172.31.143.146:2379,https://172.31.143.147:2379,https://172.31.143.149:2379" cluster-health
#查看部署到log日志  tail /var/log/messages -f
#查看部署到etcd日志 journalctl -xe     service status etcd
"