#!/bin/bash
#用于在master上执行
WORK_DIR=/opt/kubernetes
MASTER_ADDRESS=172.31.143.146
ETCD_SERVERS=https://172.31.143.148:2379,https://172.31.143.146:2379,https://172.31.143.147:2379,https://172.31.143.149:2379
HOSTS=\"172.31.143.148\",\"172.31.143.146\",\"172.31.143.147\",\"172.31.143.149\"
#you must modify “server-csr.json ” hosts ip,this contain masterip loadbalancer ip ....
CUR_WORK_DIR=echo pwd
if [ `yum list installed | grep unzip | wc -l` -ne 0 ]; then
  echo "unzip is installed"
else
  yum -y install unzip
fi
if [ ! -d "$WORK_DIR/bin" ]; then
  mkdir -p $WORK_DIR/bin
fi
if [ ! -d "$WORK_DIR/cfg" ]; then
  mkdir -p $WORK_DIR/cfg
fi
if [ ! -d "$WORK_DIR/ssl" ]; then
  mkdir -p $WORK_DIR/ssl
fi
if [ ! -d "$WORK_DIR/logs" ]; then
  mkdir -p $WORK_DIR/logs
fi
if [ `getenforce`  != 'Disabled' ]; then
  echo "you must close selinux ,/etc/selinux/config 修改selinux=disabled，重启电脑"
  exit 0
fi
if [ ! -f "$WORK_DIR/bin/kube-apiserver" ];then
  wget https://dl.k8s.io/v1.13.5/kubernetes-server-linux-amd64.tar.gz
  tar xvf kubernetes-server-linux-amd64.tar
  cp kubernetes/server/bin/kubectl /usr/bin/
  mv kubernetes/server/bin/{kube-apiserver,kube-controller-manager,kube-scheduler,kubectl,kubeadm} $WORK_DIR/bin/
  rm -rf kubernetes
  rm -rf kubernetes-server-linux-amd64.tar
else
  echo "api server existed"
fi

echo "Debug mode to stop firewalld you must usr firewalld in production"
service iptables stop 
service firewalld stop 
systemctl disable firewalld

echo "rm -rf $WORK_DIR/ssl/*"
rm -rf $WORK_DIR/ssl/*

systemctl stop kube-apiserver
systemctl stop kube-scheduler
systemctl stop kube-controller-manager

echo "cat > ca-config.json"
cat > ca-config.json <<EOF
{
  "signing": {
    "default": {
      "expiry": "87600h"
    },
    "profiles": {
      "kubernetes": {
        "expiry": "87600h",
        "usages": [
          "signing",
          "key encipherment",
          "server auth",
          "client auth"
        ]
      }
    }
  }
}
EOF
echo "cat > ca-csr.json"
cat > ca-csr.json <<EOF
{
  "CN": "kubernetes",
  "key": {
      "algo": "rsa",
      "size": 2048
  },
  "names": [
    {
      "C": "CN",
      "L": "Beijing",
      "ST": "Beijing",
      "O": "k8s",
      "OU": "System"
    } 
  ]
}
EOF

echo "cfssl gencert -initca ca-csr.json | cfssljson -bare ca -"
cfssl gencert -initca ca-csr.json | cfssljson -bare ca -


#--------------------------
echo "cat > server-csr.json"
cat > server-csr.json <<EOF
{
  "CN": "kubernetes",
  "hosts": [
    "10.0.0.1",
    "127.0.0.1",
	$HOSTS,
    "kubernetes",
    "kubernetes.default",
    "kubernetes.default.svc",
    "kubernetes.default.svc.cluster",
    "kubernetes.default.svc.cluster.local"
  ],
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "CN",
      "L": "Beijing",
      "ST": "Beijing",
      "O": "k8s",
      "OU": "System"
    }
  ]
}
EOF

echo "cfssl gencert  -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes server-csr.json | cfssljson -bare server"
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes server-csr.json | cfssljson -bare server

#--------------------------
# echo "cat > server-csr.json"
# cat > admin-csr.json <<EOF
# {
#   "CN": "kuberadminnetes",
#   "hosts": [],
#   "key": {
#     "algo": "rsa",
#     "size": 2048
#   },
#   "names": [
#     {
#       "C": "CN",
#       "L": "Beijing",
#       "ST": "Beijing",
#       "O": "system:masters",
#       "OU": "System"
#     }
#   ]
# }
# EOF

# echo "cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes admin-csr.json | cfssljson -bare admin"
# cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes admin-csr.json | cfssljson -bare admin

#--------------------------
cat > kube-proxy-csr.json <<EOF
{
  "CN": "system:kube-proxy",
  "hosts": [],
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
     {
        "C": "CN",
        "L": "BeiJing",
        "ST": "BeiJing",
        "O": "k8s",
        "OU": "System"
    } 
  ]
}
EOF
echo "cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes kube-proxy-csr.json | cfssljson -bare kube-proxy"
cfssl gencert -ca=ca.pem -ca-key=ca-key.pem -config=ca-config.json -profile=kubernetes kube-proxy-csr.json | cfssljson -bare kube-proxy

#--------------------------
echo "mv ca*pem server*pem $WORK_DIR/ssl/"
mv ca-config.json ca-csr.json server-csr.json  kube-proxy-csr.json ca*pem server*pem  kube-proxy*pem ca.csr server.csr kube-proxy.csr  $WORK_DIR/ssl/

#--------------------------
#创建 TLS Bootstrapping Token
# BOOTSTRAP_TOKEN=$(head -c 16 /dev/urandom | od -An -t x | tr -d ' ')
BOOTSTRAP_TOKEN=12a3447bf98ace692a3ad3c3b7cb0f36

cat > $WORK_DIR/cfg/token.csv <<EOF
${BOOTSTRAP_TOKEN},kubelet-bootstrap,10001,"system:kubelet-bootstrap"
EOF

#--------------------------
echo "cat >$WORK_DIR/cfg/kube-apiserver"
cat >$WORK_DIR/cfg/kube-apiserver <<EOF
KUBE_APISERVER_OPTS="--logtostderr=false \
--log-dir=$WORK_DIR/logs/ \
--v=4 \
--etcd-servers=$ETCD_SERVERS \
--bind-address=$MASTER_ADDRESS \
--secure-port=6443 \
--advertise-address=$MASTER_ADDRESS \
--allow-privileged=true \
--service-cluster-ip-range=10.0.0.0/24 \
--service-node-port-range=30000-50000 \
--enable-admission-plugins=NamespaceLifecycle,LimitRanger,ServiceAccount,ResourceQuota,NodeRestriction \
--authorization-mode=RBAC,Node \
--enable-bootstrap-token-auth \
--token-auth-file=$WORK_DIR/cfg/token.csv \
--tls-cert-file=$WORK_DIR/ssl/server.pem \
--tls-private-key-file=$WORK_DIR/ssl/server-key.pem \
--client-ca-file=$WORK_DIR/ssl/ca.pem \
--service-account-key-file=$WORK_DIR/ssl/ca-key.pem \
--etcd-cafile=/opt/etcd/ssl/ca.pem \
--etcd-certfile=/opt/etcd/ssl/server.pem \
--etcd-keyfile=/opt/etcd/ssl/server-key.pem"
EOF

echo "cat >/usr/lib/systemd/system/kube-apiserver.service"
cat >/usr/lib/systemd/system/kube-apiserver.service <<EOF
[Unit]
Description=Kubernetes APIServer
Documentation=https://github.com/kubernetes/kubernetes

[Service]
EnvironmentFile=-$WORK_DIR/cfg/kube-apiserver
ExecStart=$WORK_DIR/bin/kube-apiserver \$KUBE_APISERVER_OPTS
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable kube-apiserver
systemctl start kube-apiserver
sleep 2
systemctl status kube-apiserver

echo "cat >$WORK_DIR/cfg/kube-scheduler"
cat >$WORK_DIR/cfg/kube-scheduler <<EOF
KUBE_SCHEDULER_OPTS="--logtostderr=false \
--log-dir=$WORK_DIR/logs \
--v=4 \
--master=127.0.0.1:8080 \
--leader-elect"
EOF

echo "cat >/usr/lib/systemd/system/kube-scheduler.service"
cat >/usr/lib/systemd/system/kube-scheduler.service <<EOF
[Unit]
Description=Kubernetes Scheduler
Documentation=https://github.com/kubernetes/kubernetes

[Service]
EnvironmentFile=-/opt/kubernetes/cfg/kube-scheduler
ExecStart=/opt/kubernetes/bin/kube-scheduler \$KUBE_SCHEDULER_OPTS
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable kube-scheduler
systemctl start kube-scheduler
sleep 2
systemctl status kube-scheduler

echo "cat >$WORK_DIR/cfg/kube-controller-manager"
cat >$WORK_DIR/cfg/kube-controller-manager <<EOF
KUBE_CONTROLLER_MANAGER_OPTS="--logtostderr=false \
--log-dir=$WORK_DIR/logs \
--v=4 \
--master=127.0.0.1:8080 \
--leader-elect=true \
--address=127.0.0.1 \
--service-cluster-ip-range=10.0.0.0/24 \
--cluster-name=kubernetes \
--cluster-signing-cert-file=$WORK_DIR/ssl/ca.pem \
--cluster-signing-key-file=$WORK_DIR/ssl/ca-key.pem \
--root-ca-file=$WORK_DIR/ssl/ca.pem \
--service-account-private-key-file=$WORK_DIR/ssl/ca-key.pem"
EOF

echo "cat >/usr/lib/systemd/system/kube-controller-manager.service"
cat >/usr/lib/systemd/system/kube-controller-manager.service <<EOF
[Unit]
Description=Kubernetes Controller Manager
Documentation=https://github.com/kubernetes/kubernetes

[Service]
EnvironmentFile=-$WORK_DIR/cfg/kube-controller-manager
ExecStart=$WORK_DIR/bin/kube-controller-manager \$KUBE_CONTROLLER_MANAGER_OPTS
Restart=on-failure

[Install]
WantedBy=multi-user.target
EOF

systemctl daemon-reload
systemctl enable kube-controller-manager
systemctl start kube-controller-manager
sleep 2
systemctl status kube-controller-manager

#------------------------------
#将kubelet-bootstrap用户绑定到系统集群角色
kubectl delete clusterrolebinding kubelet-bootstrap
kubectl create clusterrolebinding kubelet-bootstrap \
--clusterrole=system:node-bootstrapper \
--user=kubelet-bootstrap
cd $WORK_DIR/ssl
# 创建kubelet bootstrapping kubeconfig
export KUBE_APISERVER="https://$MASTER_ADDRESS:6443"

# 设置集群参数
kubectl config set-cluster kubernetes \
--certificate-authority=$WORK_DIR/ssl/ca.pem \
--embed-certs=true \
--server=${KUBE_APISERVER} \
--kubeconfig=bootstrap.kubeconfig
  
# 设置客户端认证参数
kubectl config set-credentials kubelet-bootstrap \
--token=${BOOTSTRAP_TOKEN} \
--kubeconfig=bootstrap.kubeconfig

#设置上下文参数
kubectl config set-context default \
--cluster=kubernetes \
--user=kubelet-bootstrap \
--kubeconfig=bootstrap.kubeconfig
  
echo "kubectl config use-context default --kubeconfig=bootstrap.kubeconfig"
# 设置默认上下文
kubectl config use-context default --kubeconfig=bootstrap.kubeconfig
mv bootstrap.kubeconfig $WORK_DIR/cfg/
# ---------------------------
# 创建kube-proxy kubeconfig 文件

kubectl config set-cluster kubernetes \
--certificate-authority=$WORK_DIR/ssl/ca.pem \
--embed-certs=true \
--server=${KUBE_APISERVER} \
--kubeconfig=kube-proxy.kubeconfig
  
kubectl config set-credentials kube-proxy \
--client-certificate=$WORK_DIR/ssl/kube-proxy.pem \
--client-key=$WORK_DIR/ssl/kube-proxy-key.pem \
--embed-certs=true \
--kubeconfig=kube-proxy.kubeconfig
  
kubectl config set-context default \
--cluster=kubernetes \
--user=kube-proxy \
--kubeconfig=kube-proxy.kubeconfig

echo "kubectl config use-context default --kubeconfig=kube-proxy.kubeconfig"
kubectl config use-context default --kubeconfig=kube-proxy.kubeconfig
mv kube-proxy.kubeconfig $WORK_DIR/cfg/

cd $CUR_WORK_DIR
#-------------------------
echo "
#发送 kube-proxy.kubeconfig和bootstrap.kubeconfig 到所有node机器上
#scp /opt/kubernetes/cfg/{kube-proxy.kubeconfig,bootstrap.kubeconfig} root@192.168.1.111:/opt/kubernetes/cfg/
#发送 下载的kubernetes包中server/bin/kubelet,server/bin/kube-proxy 到node上
#scp /download/kubernetes/server/bin/{kubelet,kube-proxy} root@192.168.1.111:/opt/kubernetes/bin/
#kubelet 安装完毕后 会向kube-apiserver申请证书，使用命令查看
#kubectl get csr
#使用命令同意证书申请
#kubectl certificate approve XXXXID
#kubectl get node
"